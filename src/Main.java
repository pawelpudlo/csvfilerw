import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args){
        String name = "a.csv";

        int[][] data = {{1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9},
                {1,2,3,4,5,6,7,8,9}
        };

        saveFile(name,data);

        int[][] read = readFile(name);

        soutFile(read);

    }

    public static void saveFile(String fileName, int[][] data){
        Path path = Paths.get(fileName);
        ArrayList out = new ArrayList();

        for(int[] seria : data){
            String s = Arrays.toString(seria);

            s = s.replace("[","");
            s = s.replace("]","");

            out.add(s);
        }

        try {
            Files.write(path,out);
        }catch (IOException ex){
            System.out.println("You don't can write file");
            System.out.println(ex.getMessage());
        }
    }

    public static int[][] readFile(String fileName){

        Path path = Paths.get("file.csv");

        List read = new ArrayList();

        try{
            read =  Files.readAllLines(path);
        }catch (IOException ex){
            System.out.println("Not find file");
        }

        int[][] dataRead = new int[read.size()][];
        int numberLine = 0;

        for(Object line : read){
            String[] lineDataString =  String.valueOf(line).split(",");

            int[] lineInt = new int[lineDataString.length];

            for(int i=0; i < lineInt.length; i++) {
                lineInt[i] = Integer.parseInt(lineDataString[i]);
            }

            dataRead[numberLine] = lineInt;

            numberLine++;

        }
        return dataRead;
    }

    public static void soutFile(int[][] data){
        for(int[] i : data){
            System.out.println(" " + Arrays.toString(i) + " ");
        }
    }


}
